<?php // Stan 18 ноября 2004г.

  // Здесь определены имена файлов без расширений
  $this->message_file = 'message';
  $this->header_file  = 'header';
  $this->footer_file  = 'topic';
  // При определенном событии
  $this->sent_file    = 'sent';
  $this->upload_file  = 'upload';
  $this->none_file    = 'none';
  // Здесь определены расширения
  $this->html_ext     = '.htm';
  $this->php_ext      = '.php';

  // Опции отображения
  $this->view['theme'] = 'contact';       // название темы
  $this->view['lang']  = 'ru';            // язык
  $this->view['cp']    = 'utf8';          // кодовая страница
  //$this->view['bgd']   = '';

  // Опции вывода сообщений
  $this->view['from'] = 0;  // начинает с первого сообщения
  $this->view['num'] = 10;  // кол-во выводимых сообщений
  $this->view['by']   = 3;  // сортировка, номер поля в БД (с 1)
  $this->view['up']   = 0;  // порядок: 1-возрастание, 0-убывание

  // Чередование стилей сообщений
  $this->tclass[]  = 'msg1';    // стиль
  $this->tclass[]  = 'msg2';
  //$this->bgcolor[] = '#ecf2fd'; // фон сообщений
  //$this->bgcolor[] = '#f0f8ff';
  //$this->fgcolor[] = '#000000'; // цвет сообщений

?>

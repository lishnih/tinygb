<?php //Stan http://usede.net 9 апреля 2004г.

class Guestbook {
  var $classname = 'Guestbook'; // тип класса
  var $ver = 110;               // версия
  //var $message_sent = 0;      // сообщение не получено

  function Guestbook ( $id = 0, $cat = '' ) {
    // $id  - это число (БД) или строка (file)
    // $cat - это имя файла с настройками
    setlocale ( LC_TIME, "RU" );
    // setlocale( LC_TIME, 'ru_RU.UTF8' );
    $this->path = dirname( __FILE__ );  // полный путь к этому каталогу
    // Внимание: все ссылки на файлы в гостевой - относительно этого файла

    // инициализируем переменные
    $this->init_variables( $id, $cat );

    // проверка на команды, сообщения и т.п.
    $this->check_environment();
  } // function
  function destroy ( ) {
    return 1;
  } // function

  function init_variables ( $id, $cat ) {
    include 'default.php';
    // считываем параметры гостевой, если же файла нет - начинаем установку
    if ( !@include "$this->path/config.php" ) include "$this->path/install/index.php";
    $this->id  = $id;   // пока не поддерживается автоопределение !!!!!!
    $this->cat = $cat;  // и передача команд через адрес !!!!!!

    switch( $config['dbtype'] ) {
      case 'mysql':
        // опции гостевой хранятся в базе данных
        $this->conn = open_usede_db();
        break;
      case 'file':
        // загружаем опции гостевой
        if ( !isset( $config['option_path'] ) || !$config['option_path'] )
          $path = $this->path;
        else
          $path = $this->path.'/'.$config['dbpath'];
        if ( !@include $path."/options-$cat.php" )
          // если файла нет запускаем настройщика
          include "$this->path/install/tuning.php";
        if ( !isset( $config['dbpath'] ) || !$config['dbpath'] )
          $path = '';
        else
          $path = $config['dbpath'];
        if ( $cat )
          $path .= "/$cat";
        $this->filename = $path."/$id.txt"; // обратите внимание: путь
        $this->cashname = $path."/cash-$id.txt";    // относительный!
        // print_rt( $this );
        // читаем кэш-данные
        // $mcash = @file( $this->cashname );
        break;
      default:
        user_error( 'Неподдерживаемый тип данных!', ERROR );
        break;
    }; // switch

    // все опции загружены в переменной 'config', сохраняем в объекте
    while ( list( $key, $val ) = each( $config ) )
      $this->config[$key] = $val;
  } // function

  function check_environment ( ) {
    if ( isset( $_GET['pic'] ) )
      $this->pic( $_GET['pic'] );

    if ( $_GET ) {
      $command_reseved = 0;
      while ( list( $key, $val ) = each( $_GET ) ) {
        if ( $key == 'p' )
          $command_reseved = 1;
        $this->view[$key] = $val;
      }; // while
      if ( $command_reseved )
        $this->pparse( $this->view['p'] );
    }; // if
  } // function

  function pic ( $field ) {
    if ( !isset( $_GET['n'] ) )
      $text = 'item not specified';
    else {
      $item = $_GET['n'];
      $m = $this->read_messages( $item, 1 );
      $text = $m[0][$field];
    }; // if
    $len = strlen( $text );
    $x = $len * 6 + 7;
    $y = 10 + 6;
    $im  = imagecreate ( $x, $y );
    $bg_color   = imagecolorallocate( $im, 200, 200, 200 );
    $text_color = ImageColorAllocate( $im, 0, 0, 0 );
    imagefilledrectangle( $im, 0, 0, 150, 30, $bg_color );
    imagestring( $im, 2, 4, 1, $text, $text_color );
    //Header( 'Content-type: image/png' );
    imagePNG( $im );
    ImageDestroy( $im );
    exit();
  }

  function is_messages ( ) {
    if ( file_exists( $this->path.'/'.$this->filename ) )
      return 1;
    else
      return 0;
  } // function

  function prepare_str( &$str ) {
    $nls  = $this->config['newline_symbol'];    // символ новой строки
    $nlsl = strlen( $nls );                     // его длина
    $nlss = $this->config['newline_separator']; // символ, который его заменит
    //echo rawurlencode( $str );
    if ( strlen( $str ) ) {

      // убираем повторение символа новой строки
      $old_str = 0;
      while ( strlen( $str ) != strlen( $old_str ) ) {
        $old_str = $str;
        $str = str_replace( $nls.$nls, $nls, $str );
      }; // while
      // убираем символ новой строки в начале
      if ( substr( $str, 0, $nlsl ) == $nls )
        $str = substr( $str, 2, strlen( $str ) - $nlsl );
      // убираем символ новой строки в конце
      if ( substr( $str, - $nlsl ) == $nls )
        $str = substr( $str, 0, strlen( $str ) - $nlsl );
      // убираем все лишние символы, заменяем символ новой строки
      $str = htmlspecialchars( Chop( trim( str_replace( $nls, $nlss, $str ) ) ) );
      //     спец.символы, повторения пробела, с концов, замена символа новой строки.

      // parse_str
      // convert_cyr_string crypt nl2br
      //include '';
      //return competent( $str );
    }; // if
    return 1; // если возращено 1 - все нормально
  }

  function save_message ( ) {
    // считываем данные из $_POST
    while ( list( $key, $val ) = each( $_POST ) ) {
      $input[$key] = $val;
      $$key = $val;
      //echo "$key: $val<br>\n";
    }; // while

    switch( $this->config['dbtype'] ) {
      case 'mysql':
        $time = time();
        $query = "insert into gb_posts (owner,time,name,text,email) values ('$this->id','$time','$name','$text','$email')";
        if ( !$result = mysql_query( $query ) )
          user_error( mysql_errno().': '.mysql_error(), ERROR );
        break;

      case 'file':
        //while ( list( $key, $val ) = each( $this->exec ) ) {
        //  $one = '';
        //  if ( $_POST[$key] )
        //    eval( $this->exec[$key] );
        //}; // while

        // инициализируем, преобразуем данные
        while ( list( $key, $val ) = each( $this->field ) )
          if ( isset( $input[$val] ) ) {            // если переменная считана и
            if ( $this->config['must_trimed'] ) {   // если необходимо - удаляем все ненужное
              $dublicate = $input[$val];            // если в строке мат - строка будет изменена,
                                                    // сохраним ее чтобы отправить администратору
              if ( $this->prepare_str( $input[$val] ) ) // если мат не обнаружен
                $dublicate = '';                    // удаляем дубликат
            }; // if
          } else // if
            $input[$val] = '';

        // если заданы условия - архивируем данные
        reset( $this->field );
        while ( list( $key, $val ) = each( $this->field ) ) {
          unset( $one );
          if ( $this->encode[$key] ) {
            eval( $this->encode[$key] );
            $input[$val] = $one;
          }; // if
        }; // while

        // и, наконец, сохраняем
        $fp = @fopen( $this->path.$this->filename, 'a' );
        if ( $fp ) {
          reset( $this->field );
          while ( list( $key, $val ) = each( $this->field ) )
            fputs( $fp, $input[$val]."\n" );
            //echo $input[$val].' - '.$val."<br><br>\n";
          fclose( $fp );
        } else // if
          user_error( "Ошибка при открытии файла '$filename' для записи!", ERROR );
        break;
    }; // switch
    $this->message_sent = 1;    // устаналиваем флаг - сообщение было отправлено
    return 1;
  } // function

  function save_file ( $file ) {
    global $home_path, $home_url, $comm;
    $file = $_FILES["file"];
    $dest = "pp/pict/$file[name]";
    if ( move_uploaded_file ( $file['tmp_name'], "$home_path/$dest" ) ) {
      //$this->page_message = "<a href=\"$home_url/$dest\">$home_url/$dest</a>";
      if ( $fp = fopen ( "$this->path/log.txt", 'a+') ) {
        $str = "$file[name] - {$comm['addr']}\n";
        fwrite ( $fp, $str );
        fclose ( $fp );
      }; // if
      $this->page_message = "Загружен файл:<br>$home_url/$dest";
      return 1;
    } else {
      $this->page_message = 'Файл не загружен!';
      return 0;
    };
/*_FILES["file"] Array
(
    [name] => Документ блокнота.txt
    [type] => text/plain
    [tmp_name] => H:\WINNT\Temp\php13A.tmp
    [error] => 0
    [size] => 356
)*/
  } // function

  function read_messages ( $from, $num, $by = 0, $up = 1 ) {
    $result = array();
    switch( $this->config['dbtype'] ) {
      case 'mysql':
        $ordering  = ( $by ) ? "ORDER BY $by" : '';
        $ordering .= ( $up ) ? '' : ' DESC';
        $query = ( $num == 1 ) ? "SELECT * FROM gb_posts WHERE post_id = $from" :
                                 "SELECT * FROM gb_posts WHERE owner = '$this->id'$ordering LIMIT $from, $num";
        if ( !$lresult = mysql_query( $query ) )
          user_error( mysql_errno().': '.mysql_error(), ERROR );
        while ( $row = mysql_fetch_array( $lresult ) )
          $result[] = $row;
        mysql_free_result( $lresult );
        break;

      case 'file':
        $strmassive = @file( $this->path.$this->filename );

        $i = $ind = 0;  // i - сообщения; ind - считанные данные
        reset( $this->field );
        while ( isset( $strmassive[$ind]) ) {   // просматриваем считанные данные
          $one = $strmassive[$ind++];   // считываем строку
          $key =  key( $this->field );  // ключ строки

          // разархивируем данные
          if ( $this->decode[$key] )
            eval( $this->decode[$key] );
          $nlss = $this->config['newline_separator'];
          $nls  = $this->config['newline_symbol'].$this->config['line_separator'];
          $one  = str_replace( $nlss, $nls, $one );

          $lresult[$i][current( $this->field )] = $one;
          if ( !next( $this->field ) ) {
            reset( $this->field );
            $i++;
          }; // if
        }; // while
        $to = ( $i < $to ) ? $i : $to;  // наибольший номер сообщения

        if ( isset( $lresult ) ) {      // если сообщения есть, то
          // сортируем по критериям
          function cmp( $a, $b ) {
            $order = $GLOBALS["cmp_order"];
            $up = $GLOBALS["cmp_up"];
            $al = strtolower( $a[$order] );
            $bl = strtolower( $b[$order] );
            if ( $al == $bl )
              return 0;
            if ( $al > $bl )
              return ( $up ) ? 1 : -1;
            else
              return ( $up ) ? -1 : 1;
          }; // function

          if ( $by ) {
            $GLOBALS["cmp_order"] = $this->field[$by-1];
            $GLOBALS["cmp_up"] = $up;
            usort( $lresult, 'cmp' );
          };
          //usort( $lresult, 'strnatcmp' ); // неплохая замена но только для строк

          $this->count = $i;   // сохраняем кол-во сообщений

//        for ( $i = $from - 1; $i < $to; $i++ ) {
            $result = $lresult;
//        }; // for

        }; // if
        break;
    }; // switch

    return $result;
  } // function

  function pparse ( $handle ) {
    echo $this->parse( $handle );
    return 1;
  }

  function parse_file ( $filename ) {
    @include $filename.$this->php_ext;
    $data = addslashes( @implode( '', @file( $filename.$this->html_ext ) ) );
    eval( '$data = "'."$data".'";' );
    return $data;
  }

  function parse ( $handle ) {
    if ( $_POST ) $this->save_message( $_POST );
    if ( $_FILES ) $this->save_file( $_FILES );
    $id         = $this->id;
    $cat        = $this->cat;
    $theme_path = "$this->path/templates/{$this->view['theme']}";
    echo $from = ( isset( $_GET['from'] ) ) ? ( $_GET['from'] - 1 ) : $this->view['from'];	// версия 111
    $num        = $this->view['num'];
    $by         = $this->view['by'];
    $up         = $this->view['up'];
    $style      = current( $this->tclass );

    switch( $handle ) {
      case 'event':
        if ( $_POST ) {
          $body = $this->parse_file( "$theme_path/$this->sent_file" );
          break;
        }; // if
        if ( $_FILES ) {
          ;
        };

      case 'messages':
        $result = $this->read_messages ( $from, $num, $by, $up );
        $to = count( $result );
        // Загружаем начало страницы
        $body = $this->parse_file( "$theme_path/$this->header_file" );
        // Загружаем тело страницы с сообщениями
        if ( $result ) {
          $data = addslashes( @implode( '', @file( "$theme_path/$this->message_file$this->html_ext" ) ) );
          for ( $i = $from; $i < $to; $i++ ) {
            // готовим переменные для вывода
            $row = $result[$i];
            while ( list( $key, $val ) = each( $row ) )
              $$key = $val;
            @include "$theme_path/$this->message_file$this->php_ext";
            eval( '$body .= "'."$data".'";' );
            if ( !next( $this->tclass ) )
              reset( $this->tclass );
            $style = current( $this->tclass );
          }; // for
        } else {
          @include "$theme_path/$this->none_file$this->php_ext";
          $data = addslashes( @implode( '', @file( "$theme_path/$this->none_file$this->html_ext" ) ) );
          eval( '$body .= "'."$data".'";' );
        }; // if
        // Загружаем окончание страницы
        $body .= $this->parse_file( "$theme_path/$this->footer_file" );
        return $body;
        break;

      default:
//        return parse_file( "$theme_path/$handle" );
    @include $filename.$this->php_ext;
    $data = addslashes( @implode( '', @file( $filename.$this->html_ext ) ) );
    eval( '$data = "'."$data".'";' );
    return $data;
        break;
    }; // switch

  } // function

}; // class

?>

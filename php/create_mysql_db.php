<?php // Stan 6 июня 2005г.

  // Загружаем основное расширение php
  include 'phpcommon/common.php';
  include 'const.php';

  $conn = open_usede_db();
  // $db_list = mysql_list_dbs(); $i = 0; $cnt = mysql_num_rows($db_list);
  // while ($i < $cnt) { echo mysql_db_name($db_list, $i) . "\n"; $i++; }

  $sql = <<<EOT
CREATE TABLE IF NOT EXISTS `gb_posts` (
  `post_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `owner` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `time` int(11) NOT NULL DEFAULT '0',
  `name` varchar(32) NOT NULL DEFAULT '',
  `text` text NOT NULL,
  `email` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
EOT;

  $result = mysql_query( $sql ) or user_error( mysql_errno().': '.mysql_error(), ERROR );

  mysql_close( $conn );
?>

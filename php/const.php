<?php // Stan 27 августа 2004г.
// в этом файле указаны по возможности все пути
// не должны оканчиваться слэшем

  #### URLs ####
  // стартовая страница
  define( 'HOME_URL',           'http://localhost' );

  #### Pathes ####
  // Наша HTML-страница хранится здесь
  define( 'HOME_PATH',          '/opt/home/php/public_html/localhost/gb' );
  // Этот файл и все программы php находятся здесь
  define( 'PHP_PATH',           '/opt/home/php/public_html/localhost/gb/php' );
  // пакеты скриптов
  define( 'GBOOK_PATH',         PHP_PATH . '/gbook' );      // гостевая книга
  define( 'ACTIVEGB_PATH',      PHP_PATH . '/gbook' );      // гостевая книга
  define( 'KCAPTCHA_PATH',      PHP_PATH . '/kcaptcha' );   // PHP CAPTCHA
  // это чтобы проше вызывать в include
  define( 'LOGGER_PACKET',      '' ); // не используется
  define( 'GBOOK_PACKET',       GBOOK_PATH . '/gb-class.php' );
  define( 'ENGINE_PACKET',      '' ); // не используется
  define( 'ACTIVEGB_PACKET',    GBOOK_PATH . '/class_gb.php' );
  define( 'KCAPTCHA_PACKET',    KCAPTCHA_PATH . '/kcaptcha.php' );

  #### MySQL ####
  define( 'USE_DB_HOST',        'localhost' );
  define( 'USE_DB_NAME',        'gb' );
  define( 'USE_DB_USER',        'root' );
  define( 'USE_DB_PASSWD',      '54321' );

///////////////////////////////////////////////////
/////////////// Функция открытия БД ///////////////
///////////////////////////////////////////////////
  function open_usede_db ( ) {
    $conn = @mysql_connect( USE_DB_HOST, USE_DB_USER, USE_DB_PASSWD )
      or user_error( mysql_errno().': '.mysql_error(), ERROR );
    mysql_select_db( USE_DB_NAME, $conn )
      or user_error( mysql_errno().': '.mysql_error(), ERROR );
    mysql_query( 'SET NAMES UTF8' );
    return $conn;
  } // function
?>
